require "rake/clean"
require "shellwords"

if not ENV.key?("ENVIRONMENT") then
  ENV["ENVIRONMENT"] = "dev"
end

BUILD_DIR = "build"
DEFAULT_DIR = "web/sites/default"
PROFILE = "data"
SETTINGS_SOURCE_REL = "provisioning/settings.#{ENV['ENVIRONMENT']}.php"
SETTINGS_TARGET_REL = "#{DEFAULT_DIR}/settings.local.php"
STAGE = ""
PROD = ""
DEPLOY_TARGET_STAGE = ""
DEPLOY_TARGET_PROD = ""
CF_USER_ID = "cloudflare@agaric.com"
CF_ZONE_ID = ""

if ENV["ENVIRONMENT"] == "dev" then
  SETTINGS_SOURCE = SETTINGS_SOURCE_REL
  SETTINGS_TARGET = SETTINGS_TARGET_REL
else
  SETTINGS_SOURCE = "#{BUILD_DIR}/#{SETTINGS_SOURCE_REL}"
  SETTINGS_TARGET = "#{BUILD_DIR}/#{SETTINGS_TARGET_REL}"
end

if ENV["ENVIRONMENT"] == "dev" then

  desc "Copy database and files from live site to local."
  task :sync_from_prod do
    sh "./vendor/bin/drush sql-dump > /tmp/paranoia.sql"
    sh "./vendor/bin/drush -y sql-sync @production @self"
    sh "./vendor/bin/drush -y rsync @production:%files/ @self:%files"
    sh "./vendor/bin/drush user-password admin --password=admin"
  end

  desc "Apply updates locally."
  task :update_local do
    sh "./vendor/bin/drush -y cim"
    sh "./vendor/bin/drush -y updb"
    sh "./vendor/bin/drush cr"
  end
end

if ENV["ENVIRONMENT"] == "stage" then
  DEPLOY_TARGET = "#{STAGE}:#{DEPLOY_TARGET_STAGE}"
elsif ENV["ENVIRONMENT"] == "production" then
  DEPLOY_TARGET = "#{PROD}:#{DEPLOY_TARGET_PROD}"
end

CLEAN.include(["#{BUILD_DIR}/*"])

file SETTINGS_TARGET => SETTINGS_SOURCE do
  cp SETTINGS_SOURCE, SETTINGS_TARGET
end

task :test => ["test:behat"]

task :vendor do
  sh "composer install"
end

desc "Compile CSS."
task :css do
  sh "sass web/themes/hyphae/sass/styles.scss web/themes/hyphae/css/styles.css"
end

task :default => ["test"]
desc "Install Drupal."
task :drupal => [:vendor, SETTINGS_TARGET] do
  sh "./vendor/bin/drush -r #{Dir.pwd.shellescape}/web -y si #{PROFILE} install_configure_form.update_status_module='[0, 0]'"
  chmod_R "u+w", DEFAULT_DIR
end

task :build  do
  mkdir_p BUILD_DIR
  sh "git archive HEAD | tar -x -C #{BUILD_DIR}"
  sh "composer install -d #{BUILD_DIR} --no-dev"
  cp SETTINGS_SOURCE, SETTINGS_TARGET
  sh "sass #{BUILD_DIR}/web/themes/hyphae/sass/styles.scss #{BUILD_DIR}/web/themes/hyphae/css/styles.css"
end

task :deploy_config => [:build] do
  sh "rsync -rz --delete #{BUILD_DIR}/config/ #{DEPLOY_TARGET}/config/"
end

task :deploy_web => [:build] do
  sh "rsync -rz --delete --exclude sites/default/files #{BUILD_DIR}/web/ #{DEPLOY_TARGET}/web/"
end

task :deploy_vendor => [:build] do
  sh "rsync -rz --delete --links #{BUILD_DIR}/vendor/ #{DEPLOY_TARGET}/vendor/"
end

task :deploy => [:deploy_config, :deploy_vendor, :deploy_web] do
  sh "./build/vendor/bin/drush @#{ENV['ENVIRONMENT']} --alias-path=#{BUILD_DIR}/drush/ -y updb"
  sh "./build/vendor/bin/drush @#{ENV['ENVIRONMENT']} --alias-path=#{BUILD_DIR}/drush/ -y cim"
  sh "./build/vendor/bin/drush @#{ENV['ENVIRONMENT']} --alias-path=#{BUILD_DIR}/drush/ cr"
end

desc "Copy database and files from live site to stage."
task :sync_prod_to_stage do
  sh "ssh -A #{PROD} rsync -rz --stats --exclude styles \
      --exclude css --exclude js #{DEPLOY_TARGET_PROD}/web/sites/default/files/ \
      --delete #{STAGE}:#{DEPLOY_TARGET_STAGE}/web/sites/default/files/"
  sh "ssh -C #{PROD} drush -r #{DEPLOY_TARGET_PROD}/web \
      sql-dump --structure-tables-key=common | \
      ssh -C #{STAGE} drush -r #{DEPLOY_TARGET_STAGE}/web sql-cli"
end

namespace :test do
  desc "Run Behat tests."
  task :behat => [:vendor] do
    sh "./vendor/bin/behat --tags '~@javascript' --format pretty --out std --format junit --out reports"
  end

  desc "Run performance tests."
  task :performance => [:vendor] do
    sh "./vendor/bin/phpunit --bootstrap ./vendor/autoload.php --log-junit reports/performance.xml tests/"
  end
end

