<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function data_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.mass':
      $output = '<h2>' . t('Agaric data site site: Editor\'s guide') . '</h2>';

      $output .= '<h3>' . t('Image dimension ratios (width:height)') . '</h3>';
      $output .= '<ul>';
      $output .= '<li>' . t('Cover images, 4:3') . '</li>';
      $output .= '</ul>';

      $output .= '<h3>' . t('Removing a position from the job listing') . '</h3>';
      $output .= '<p>' . t('To delist a job from the <a href=":jobs">jobs page</a>, click through to the job and edit it.  At the bottom of the job edit form, checkmark "Filled".') . '</p>';

      $output .= '<h3>' . t('Featuring content') . '</h3>';
      $output .= '<ol>';
      $output .= '<li>' . t('Edit or <a href=":url">create a Feature</a>.', array(':url' => \Drupal::url('node.add', array('node_type' => 'feature')))) . '</li>';
      $output .= '<li>' . t('Use the <strong>Featured content</strong> field to reference content from the site by typing part of the content\'s title <em>or</em> use the same field to link to external content by providing a full URL.') . '</li>';
      $output .= '</ol>';

      return $output;
  }
}
