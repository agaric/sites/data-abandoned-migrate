<?php

/**
 * @file
 * Settings for the dev environment.
 */

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'drupal',
  'username' => 'root',
  'password' => '',
  'host' => 'localhost',
  'prefix' => '',
);

$settings['trusted_host_patterns'] = array(
  '^data\.local$',
);

$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

