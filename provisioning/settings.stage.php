<?php

/**
 * @file
 * Settings for the stage environment.
 */

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'drupal',
  'username' => 'root',
  'password' => 'something-is-better-than-nothing-yes?',
  'host' => 'localhost',
  'prefix' => '',
);

$settings['trusted_host_patterns'] = array(
  'stage.data.com',
);

