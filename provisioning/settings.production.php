<?php

/**
 * @file
 * Settings for the production environment.
 */

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'drupal',
  'username' => 'root',
  'password' => '',
  'host' => 'localhost',
  'prefix' => '',
);

$settings['trusted_host_patterns'] = array(
  'data.com',
  'direct.data.com',
);

